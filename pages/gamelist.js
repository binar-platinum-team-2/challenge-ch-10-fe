import react, { useEffect, useState } from "react";
import Link from "next/link";
import Footer from "../component/Footer";
import Header from "../component/Header";
import Image from "next/image";
import coming from "../public/images/coming-soon.jpeg";
import suit from "../public/images/suit.jpg";
import { useSelector, useDispatch } from "react-redux";
import dice from "../public/images/dice.jpg";
import { isplayed } from "../store/cart/userSlice";

const GameList = () => {
  const user = useSelector((state) => {
    return state.user.player;
  });
  return (
    <>
      <div className="latar">
        <div className="container">
          <Header />
          <div className="p-4">
            <h1 className="m-4 tulis-tengah">Game List</h1>
            <div className="game-list d-sm-flex justify-content-between flex-wrap px-0">
              <Link href="/gamedetail">
                <div
                  className="game-card rounded-5 bg-opacity-10 p-1 position-relative"
                  style={{ width: "18rem", height: "20rem" }}
                >
                  <Image
                    className="rounded-5 gambar"
                    src={suit}
                    alt="game icon"
                  />
                  {user.skor !== null || user.skor !== 0 ? (
                    <p className="position-absolute fixed-top m-3">
                      Already Played
                    </p>
                  ) : null}
                  <div className="card-body">
                    <h5 className="card-title w-100">Janken</h5>
                  </div>
                </div>
              </Link>
              <Link href="/gamedetail1">
                <div
                  className="game-card rounded-5 bg-opacity-10 p-1 position-relative"
                  style={{ width: "18rem", height: "20rem" }}
                >
                  <Image
                    className="rounded-5 gambar"
                    src={dice}
                    alt="game icon"
                  />
                  {user.isplayed == true ? (
                    <p className="position-absolute fixed-top m-3">
                      Already Played
                    </p>
                  ) : null}
                  <div className="card-body">
                    <h5 className="card-title w-100">Dice</h5>
                  </div>
                </div>
              </Link>

              <div
                className="p-1 game-card rounded-5"
                style={{ width: "18rem" }}
              >
                <Image
                  className="rounded-5 gambar"
                  src={coming}
                  alt="game icon"
                />
                <div className="card-body">
                  <h5 className="card-title">Coming Soon</h5>
                  <p className="card-text"></p>
                  {/* <a className="btn btn-primary">Go somewhere</a> */}
                </div>
              </div>
              <div
                className="p-1 game-card rounded-5"
                style={{ width: "18rem" }}
              >
                <Image
                  className="rounded-5 gambar"
                  src={coming}
                  alt="game icon"
                />
                <div className="card-body">
                  <h5 className="card-title">Coming Soon</h5>
                  <p className="card-text"></p>
                  {/* <a className="btn btn-primary">Go somewhere</a> */}
                </div>
              </div>
              <div
                className="p-1 game-card rounded-5"
                style={{ width: "18rem" }}
              >
                <Image
                  className="rounded-5 gambar"
                  src={coming}
                  alt="game icon"
                />
                <div className="card-body">
                  <h5 className="card-title">Coming Soon</h5>
                  <p className="card-text"></p>
                  {/* <a className="btn btn-primary">Go somewhere</a> */}
                </div>
              </div>
              <div
                className="game-card rounded-5 p-1"
                style={{ width: "18rem" }}
              >
                <Image
                  className="rounded-5 gambar"
                  src={coming}
                  alt="game icon"
                />
                <div className="card-body">
                  <h5 className="card-title">Coming Soon</h5>
                  <p className="card-text"></p>
                  {/* <a className="btn btn-primary">Go somewhere</a> */}
                </div>
              </div>
            </div>
          </div>

          <Footer />
        </div>
      </div>
    </>
  );
};

export default GameList;

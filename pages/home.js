import react, { useEffect, useState } from "react";
import Footer from "../component/Footer";
import Header from "../component/Header";
import suit from "../public/images/suit.jpg";
import Image from "next/image";
import { useRouter } from "next/router";
import { useSelector,useDispatch } from "react-redux";
import { getUserAsync } from "../store/cart/alldataSlice";
import { getPlayerAsync } from "../store/cart/userSlice";

export default function Home() {
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector((state) => {
    return state.user.player;
  });

  useEffect(() => {
    dispatch(getUserAsync());
    dispatch(getPlayerAsync());
  }, []);

  console.log("user",user);
  console.log("user",user)
  return (
    <>
      <div className="latar">
        <div className="container">
          <Header />
          <div className="container home-content rounded p-3 shadow">
            <h4 className="home-left text-light">Hi,Welcome {user.username}</h4>
            <p className="home-left text-light">Recomended Game For You</p>
            <div className="home-box-game mb-3 rounded border">
              <div className="p-3">
                <Image src={suit} alt="gambar suit"/>
              </div>
              <div className="p-3 text-light home-detail">
                <h3 className="text-white">Rock-Paper-Scissors</h3>
                <p>Our most played game</p>
                <p>
                  What is the concept of Rock Paper Scissors? Each gesture
                  defeats one and is defeated by one of the other two: rock
                  defeats scissors but is defeated by paper; paper defeats rock
                  but is defeated by scissors. The person whose gesture defeats
                  the other is selected.
                </p>
                <button
                  className="home-edit-button"
                  onClick={() => router.push("/gamelist")}
                >
                  Play Now
                </button>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
}

// export default Home;

import { Alert, Container } from "reactstrap";
import { useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Link from "next/link";
import CustomButton from "../component/Button";

export default function Register() {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [skor, setSkor] = useState(null);
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(false);

  const registerHandler = async (e) => {
    e.preventDefault()
    try {
      setIsLoading(true);
      const register = await axios.post(
        "https://fsw-team2-backend.herokuapp.com/register",
        {
          username,
          email,
          password,
          skor,
        }
      );
      router.push("/login");
      setIsLoading(false)
    } catch (err) {
      router.push("/404");
    }
  };

  return (
    <div className="background">
      <div className="form">
        <Container className="text-center title--login">
          <h1>REGISTER</h1>
        </Container>
        <form onSubmit={registerHandler}>
          <div className="mb-3">
            <label className="form-label text--style">Email address</label>
            <input
              type="email"
              className="form-control input--style"
              placeholder="ex:John@gmail.com"
              value={email}
              required
              onChange={(e) => setEmail(e.target.value)}
            ></input>
          </div>
          <div className="mb-3">
            <label className="form-label text--style">Username</label>
            <input
              type="text"
              className="form-control input--style"
              placeholder="ex:John123"
              required
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            ></input>
            <div id="emailHelp" className="form-text text--style">
              We'll never share your email with anyone else.
            </div>
          </div>
          <div className="mb-3">
            <label className="form-label text--style">Password</label>
            <input
              type="password"
              className="form-control input--style"
              placeholder="ex:1234"
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></input>
          </div>
          <CustomButton type={"submit"} isLoading={isLoading} label="Submit" />
          <Link style={{marginLeft: "5px"}} className="btn btn-primary" href="/login" disabled={isLoading}>
            Login
          </Link>
        </form>
      </div>
    </div>
  );
}
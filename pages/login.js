import { Container, Spinner } from "reactstrap";
import Link from "next/link";
import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { useSelector,useDispatch } from "react-redux";
import { getUserAsync } from "../store/cart/alldataSlice";
import { getPlayerAsync } from "../store/cart/userSlice";
import { loading } from "../store/cart/userSlice";
import CustomButton from "../component/Button";

export default function Login() {
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const load = useSelector((state) => {
    return state.user.isLoading;
  });
  const router = useRouter();
  const loginHandler = async () => {
    try {
      dispatch(loading());
      const login = await axios.post(
        "https://fsw-team2-backend.herokuapp.com/login",
        {
          email,
          password,
        }
      );
      localStorage.setItem("accessToken", login.data.token);

      if (!login.data.token) {
        router.push("/login");
        alert("Player Data Not Found");
        dispatch(loading());
        return;
      }
      if (login.data) {
        dispatch(loading());
      }
      router.push("/home");
    } catch (err) {
      router.push("/login");
    }
  };


  return (
    // <form>
    <div className="background">
      <div className="form">
        <Container className="text-center margin--header">
          <h1 className="title--login">LOGIN</h1>
        </Container>
        <Container className="margin--form">
          <div className="mb-3">
            <label className="form-label text--style">Email address</label>
            <input
              type="email"
              className="form-control input--style "
              placeholder="ex:John@gmail.com"
              aria-describedby="emailHelp"
              disabled={load}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            ></input>
          </div>
          <div className="mb-3">
            <label className="form-label text--style">Password</label>
            <input
              type="password"
              className="form-control input--style"
              placeholder="ex:1234"
              disabled={load}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></input>
          </div>

          <Link href={load ? "#" : "/register"}>
            <button className="btn btn-primary" disabled={load}>
              Register
            </button>
          </Link>
          {" "}
          <CustomButton onClick={loginHandler} isLoading={load} label="Login" />
        </Container>
      </div>
    </div>
    // </form>
  );
}

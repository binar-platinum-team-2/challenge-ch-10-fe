import Link from "next/link";
import Header from "../component/Header";
import Footer from "../component/Footer";
import { useEffect, useState } from "react";
import Image from "next/image";
import dice from "../public/images/dice.jpg";
import { useSelector, useDispatch } from "react-redux";
import { isplayed } from "../store/cart/userSlice";
import axios from "axios";
const GameDetail = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => {
    return state.user.player;
  });
  const hit = async () => {
    try {
      dispatch(isplayed());
      const updateUser = await axios.post(
        "https://fsw-team2-backend.herokuapp.com/edit",
        {
          id: user.id,
          isplayed: true,
        }
      );
      
    alert("Game Under Maintance");
    } catch (err) {
      alert("error");
    }
  };

  console.log("user", user);
  return (
    <>
      <div className="latar">
        <div className="container">
          <Header />
          <div className="home-content">
            <h1 className="tulis-tengah">Game Detail</h1>
            <div className="game-card rounded-5 p-3 m-3">
              <div className="row g-0 align-items-center">
                <div className="col-md-4">
                  <Image
                    src={dice}
                    className="img-fluid d-block mx-auto rounded-5"
                    alt="dice game"
                  />
                </div>
                <div className="col-md-8">
                  <div className="card-body p-4">
                    <h2 className="card-title">Dice</h2>
                    <p className="card-text">
                      A dice is a small cube which has between one and six spots
                      or numbers on its sides, and which is used in games to
                      provide random numbers. In old-fashioned English, 'dice'
                      was used only as a plural form, and the singular was die,
                      but now 'dice' is used as both the singular and the plural
                      form.
                    </p>
                    <div className="game-game-bottom">
                      <div className="leaderboard w-50 p-2">
                        <h5 className="card-text">Top 3 LeaderBoard</h5>

                        <table className="table table-hover table-dark">
                          <thead>
                            <tr>
                              <th scope="col">No</th>
                              <th scope="col">Username</th>
                              <th scope="col">Score</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <td>Ricky</td>
                              <td>8888</td>
                            </tr>
                            <tr>
                              <th>2</th>
                              <td>Farhan</td>
                              <td>7654</td>
                            </tr>
                            <tr>
                              <th>3</th>
                              <td>Ilfiyanti</td>
                              <td>7644</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div className="w-50 p-2">
                        <button
                          className="rounded-5 home-edit-button w-50"
                          onClick={hit}
                        >
                          Play
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default GameDetail;

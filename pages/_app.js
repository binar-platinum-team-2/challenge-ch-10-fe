import "../styles/globals.css";
import "../styles/Pages.css";
import "../styles/Header.css";
import "../styles/Footer.css";
import "../styles/Game.css";
import "../styles/login.css";
import "../styles/Janken.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider, useSelector, useDispatch } from "react-redux";
import { store } from "../store";
import { getUserAsync } from "../store/cart/alldataSlice";
import { getPlayerAsync } from "../store/cart/userSlice";
import { useEffect } from "react";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  useEffect(() => {
    const cektoken = localStorage.getItem("accessToken");
    if (cektoken) {
      store.dispatch(getUserAsync());
      store.dispatch(getPlayerAsync());
      if (
        router.route == "/" ||
        router.route == "/login" ||
        router.route == "/register"
      ) {
        router.replace("/home");
      }
    } else if (!cektoken && router.route == "/") {
      router.replace("/");
    } else if (!cektoken && router.route == "/login") {
      router.replace("/login");
    } else if (!cektoken && router.route == "/register") {
      router.replace("/register");
    } else {
      router.replace("/login");
    }
    console.log("route", router);
  }, [router.route]);

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;

import react, { useEffect, useState } from "react";
import Footer from "../component/Footer";
import Header from "../component/Header";
import { useSelector, useDispatch } from "react-redux";
import { getUserAsync } from "../store/cart/alldataSlice";

function Home() {
  const dispatch = useDispatch();
  const alluser = useSelector((state) => {
    return state.alluser.userdata;
  });

  useEffect(() => {
    dispatch(getUserAsync());
  });
  console.log("user", alluser);
  return (
    <>
      <div className="latar">
        <div className="container">
          <Header />
          <div className="home-content rounded shadow">
            <h1 className="p-3 text-light tulis-tengah">Leaderboard</h1>
            <div className="profile-profile-box">
              <div className="profile-left-profile">
                <div className="border rounded p-3">
                  <h3 className="lead-badge-req">
                    You can get some badge when you reach requirement score
                  </h3>
                  <ul>
                    <li>
                      <h5>Gold Badge When Your Total Score More Than 1000</h5>
                    </li>
                    <li>
                      <h5>Silver Badge When Your Total Score More Than 500</h5>
                    </li>
                    <li>
                      <h5>Bronze Badge When Your Total Score More Than 200</h5>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="profile-right-profile">
                <table className="table table-hover table-dark">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Username</th>
                      <th scope="col">Score</th>
                    </tr>
                  </thead>
                  <tbody>
                    {alluser.slice(0, 5).map((user, index) => (
                      <tr key={index}>
                        <th>{index + 1}</th>
                        <td>{user.username}</td>
                        <td>{user.skor}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <Footer />
        </div>
      </div>
    </>
  );
}

export default Home;

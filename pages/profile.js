import React, { useState, useEffect } from "react";
import Header from "../component/Header";
import Footer from "../component/Footer";
import face from "../public/images/face.jpg";
import Link from "next/link";
import Image from "next/image";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { updateplayer } from "../store/cart/userSlice";
import { loading } from "../store/cart/userSlice";
import { useRouter } from "next/router";

function Profile() {
  const dispatch = useDispatch();
  const router = useRouter

  const user = useSelector((state) => {
    return state.user.player;
  });

  const load = useSelector((state) => {
    return state.user.isLoading;
  });

  const [edit, setEdit] = useState(false);
  const [edituser, seteditUser] = useState(user);
  const handleInputEdit = (event) => {
    const { name, value } = event.target;

    seteditUser({ ...edituser, [name]: value });
  };

  console.log("edit",edituser);
  const updateUser = async (e) => {
    try {
      e.preventDefault();
      setEdit(false);
      dispatch(loading());
      dispatch(updateplayer(edituser));
      const updateUser = await axios.post(
        
        "https://fsw-team2-backend.herokuapp.com/edit",
        {
          id: edituser.id,
          username: edituser.username,
          skor: edituser.skor,
          email: edituser.email,
        }
      );
      if (updateUser) {
        dispatch(loading())
      }
    }
    catch (err) {
      router.push("/404");
    }
  };

  console.log("user",user);
  return (
    <div className="latar">
      <div className="container">
        <Header />
        <div className="home-content rounded shadow">
          <h1 className="p-3 text-light tulis-tengah">Profile</h1>
          <div className="profile-profile-box">
            <div className="profile-left-profile">
              <div className="profile-badge">
                {user.skor > 1000 ? (
                  <div className="rounded-circle profile-badge-list profile-gold">
                    1000 Poin
                  </div>
                ) : null}
                {user.skor > 500 ? (
                  <div className="rounded-circle profile-badge-list profile-silver">
                    500 Poin
                  </div>
                ) : null}
                {user.skor > 200 ? (
                  <div className="rounded-circle profile-badge-list profile-bronze">
                    200 Poin
                  </div>
                ) : null}
              </div>

              <div className="border rounded-circle profile-foto">
                <Image src={face} className="profile-pict" />
              </div>
            </div>
            {edit ? (
              <div className="profile-right-profile">
                <div className="mb-3">
                  <label className="form-label">Username</label>
                  <input
                    className="form-control shadow"
                    name="username"
                    value={edituser.username}
                    onChange={handleInputEdit}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Email</label>
                  <input
                    className="form-control shadow"
                    value={edituser.email}
                    name="email"
                    onChange={handleInputEdit}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Password</label>
                  <input
                    className="form-control shadow"
                    value="***************"
                    name="password"
                    onChange={handleInputEdit}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Score</label>
                  <input
                    className="form-control shadow"
                    value={edituser.skor}
                    name="score"
                    onChange={handleInputEdit}
                  />
                </div>
                <button
                  className="home-edit-button w-100 rounded-3"
                  onClick={updateUser}
                >
                  Apply
                </button>
              </div>
            ) :
            (
              <div className="profile-right-profile">
                <div className="mb-3">
                  <label className="form-label">Username</label>
                  <input
                    className="form-control shadow"
                    name="username"
                    value={user.username}
                    onChange={handleInputEdit}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Email</label>
                  <input
                    className="form-control shadow"
                    value={user.email}
                    name="email"
                    onChange={handleInputEdit}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Password</label>
                  <input
                    className="form-control shadow"
                    value="***************"
                    name="password"
                    onChange={handleInputEdit}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Score</label>
                  <input
                    className="form-control shadow"
                    value={user.skor}
                    name="score"
                    onChange={handleInputEdit}
                    disabled
                  />
                </div>
                <button
                  className="home-edit-button w-100 rounded-3"
                  onClick={() => setEdit(true)}
                >
                  Edit
                </button>
              </div>
            )}
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default Profile;

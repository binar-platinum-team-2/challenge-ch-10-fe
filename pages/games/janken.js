import { useEffect } from "react";
import { useState } from "react";
import { Button } from "reactstrap";
import Link from "next/link";
import back from "../../public/assets/back.png";
import batu from "../../public/assets/batu.png";
import gunting from "../../public/assets/gunting.png";
import kertas from "../../public/assets/kertas.png";
import refresh from "../../public/assets/refresh.png";
import Image from "next/image";
import { useSelector, useDispatch } from "react-redux";
import {
  resetGame,
  userChoice,
  comChoice,
  roundFinish,
  roundPlay,
  nextRound,
  gameFinish,
  win,
  lose,
  draw,
  hasil,
} from "../../store/cart/suitSlice";
import { updatescore } from "../../store/cart/userSlice";
import axios from "axios";
import { useRouter } from "next/router";

const Janken = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => {
    return state.user.player;
  });
  const suit = useSelector((state) => {
    return state.suit;
  });
  const [currentScore, setcurrentScore] = useState(0);
  const [roundscore, setRoundscore] = useState([]);
  const router = useRouter();
  const updatePlayerScore = async (score) => {
    try {
      const updateScore = await axios.put(
        "https://fsw-teamb-backend.herokuapp.com/janken",
        {
          id: user.id,
          skor: score,
        },
        {
          headers: {
            Authorization: localStorage.getItem("accessToken"),
          },
        }
      );
      return updateScore;
    } catch (err) {
      console.log(err.message);
    }
  };

  const X = <h1 className="x">X</h1>;
  const O = <h1 className="o">O</h1>;
  const sama = <h1 className="sama">=</h1>;

  const resetgame = () => {
    dispatch(resetGame());
    setcurrentScore(0);
    setRoundscore([]);
  };
  console.log("game", suit);
  const comSelection = ["rock", "paper", "scissor"];

  const getComPick = () => {
    const randomNumber = Math.floor(Math.random() * 3);
    const randomPick = comSelection[randomNumber];
    dispatch(comChoice(randomPick));
  };
  useEffect(() => {
    determineWinner();
    if (suit.roundFinish) {
      if (suit.roundPlay === 5) {
        dispatch(gameFinish());
        dispatch(roundFinish(true));
      }
    }
  }, [suit.roundFinish]);

  useEffect(() => {
    const playerscore = user.skor;
    const totalscore = playerscore + currentScore;
    if (suit.gameFinish) {
      if (suit.score > suit.comScore) {
        dispatch(hasil("You Win the Game!!"));
        dispatch(updatescore(totalscore));
        updatePlayerScore(totalscore);
      } else if (suit.comScore > suit.score) {
        dispatch(hasil("Com Win The Game"));
        dispatch(updatescore(totalscore));
        updatePlayerScore(totalscore);
      } else if (suit.comScore > suit.score) {
        dispatch(hasil("Com Win The Game"));
        return;
      } else {
        dispatch(hasil("Draw"));
        return;
      }
    }
  }, [suit.gameFinish]);

  const backroute = () => {
    router.push("/gamedetail");
  };
  const getUserPick = (pick) => {
    if (!suit.roundFinish && !suit.gameFinish) {
      dispatch(userChoice(pick));
      getComPick();
      dispatch(roundPlay());
      if (suit.roundPlay !== 5) {
        dispatch(roundFinish(true));
      }
    }
  };

  const determineWinner = () => {
    if (suit.userPick === "scissor") {
      if (suit.comPick === "scissor") {
        dispatch(hasil("Draw"));
        roundscore.push(sama);
      } else if (suit.comPick === "rock") {
        dispatch(lose());
        roundscore.push(X);
        setcurrentScore(currentScore - 5);
      } else if (suit.comPick === "paper") {
        dispatch(win());
        setcurrentScore(currentScore + 10);
        roundscore.push(O);
      }
    } else if (suit.userPick === "rock") {
      if (suit.comPick === "scissor") {
        dispatch(win());
        setcurrentScore(currentScore + 10);
        roundscore.push(O);
      } else if (suit.comPick === "rock") {
        dispatch(hasil("Draw"));
        roundscore.push(sama);
      } else if (suit.comPick === "paper") {
        dispatch(lose());
        setcurrentScore(currentScore - 5);
        roundscore.push(X);
      }
    } else if (suit.userPick === "paper") {
      if (suit.comPick === "scissor") {
        dispatch(lose());
        setcurrentScore(currentScore - 5);
        roundscore.push(X);
      } else if (suit.comPick === "rock") {
        dispatch(win());
        setcurrentScore(currentScore + 10);
        roundscore.push(O);
      } else if (suit.comPick === "paper") {
        dispatch(hasil("Draw"));
        roundscore.push(sama);
      }
    }
  };

  return (
    <div className="gameScreen">
      <div>
        <header id="header" className="container">
          <Image id="backIcon" src={back} alt="back-icon" onClick={backroute} />
          <h1>ROCK PAPER SCISSORS</h1>
        </header>
      </div>
      <main className="container">
        {suit.gameFinish ? (
          <>
            <div className="home-box-game m-5 p-5">
              <div>
                <p id="winner" className="vs">
                  {suit.winner}
                </p>
                <h1 className="mb-5 text-light">
                  YOU GOT {currentScore} SCORE
                </h1>
                <div className="home-box-game">
                  {roundscore.map((score) => (
                    <p>{score}</p>
                  ))}
                </div>
                <button className="btn btn-primary" onClick={resetgame}>
                  Continue?
                </button>
                <Link href="/gamelist" onClick={resetgame}>
                  {" "}
                  <button className="btn btn-danger">Game Over</button>{" "}
                </Link>
              </div>
            </div>
          </>
        ) : (
          <div className="gameGrid">
            <div className="scoring">
              {" "}
              <p>Round : {suit.roundPlay}</p>
              <p>Score : {currentScore}</p>
              <div className="home-box-game">
                {roundscore.map((score) => (
                  <p>{score}</p>
                ))}
              </div>
            </div>

            <div className="playerName">
              <h3>{user.username}</h3>
            </div>
            <div
              id="pRock"
              className={suit.userPick === "rock" ? "jankenPick" : null}
              onClick={() => getUserPick("rock")}
            >
              <Image className="pRock" src={batu} alt="" />
            </div>
            <div
              id="pPaper"
              className={suit.userPick === "paper" ? "jankenPick" : null}
              onClick={() => getUserPick("paper")}
            >
              <Image className="pPaper" src={kertas} alt="" />
            </div>
            <div
              id="pScissor"
              className={suit.userPick === "scissor" ? "jankenPick" : null}
              onClick={() => getUserPick("scissor")}
            >
              <Image className="p-scissor" src={gunting} alt="" />
            </div>
            <p id="winner" className="vs">
              {suit.winner}
            </p>
            <div className="comName">
              <h3>COM</h3>
            </div>
            <div
              id="cRock"
              className={suit.comPick === "rock" ? "jankenPick" : null}
            >
              <Image className="cRock" src={batu} alt="" />
            </div>
            <div
              id="cPaper"
              className={suit.comPick === "paper" ? "jankenPick" : null}
            >
              <Image className="cPaper" src={kertas} alt="" />
            </div>
            <div
              id="cScissor"
              className={suit.comPick === "scissor" ? "jankenPick" : null}
            >
              <Image className="cScissor" src={gunting} alt="" />
            </div>

            {suit.roundFinish && (
              <Image
                className="refresh"
                src={refresh}
                alt=""
                onClick={() => dispatch(nextRound())}
                disabled={suit.gameFinish}
              />
            )}
          </div>
        )}
      </main>
    </div>
  );
};

export default Janken;

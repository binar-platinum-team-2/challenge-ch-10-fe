import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./cart/userSlice";
import alluserReducer from "./cart/alldataSlice";
import suitReducer from "./cart/suitSlice";

export const store = configureStore({
  reducer: {
    user: userReducer,
    alluser: alluserReducer,
    suit: suitReducer,
  },
});

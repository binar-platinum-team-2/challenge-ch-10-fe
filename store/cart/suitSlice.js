import { createSlice } from "@reduxjs/toolkit";
const axios = require("axios");
const initialState = {
  winner: "VS",
  score: 0,
  comScore: 0,
  userPick: "",
  comPick: "",
  roundFinish: false,
  roundPlay: 0,
  gameFinish: false,
};

export const suitSlice = createSlice({
  name: "suit",
  initialState,
  reducers: {
    resetGame: (state, action) => {
      state.winner = "VS";
      state.score = 0;
      state.comScore = 0;
      state.userPick = "";
      state.comPick = "";
      state.roundFinish = false;
      state.roundPlay = 0;
      state.gameFinish = false;
    },
    userChoice: (state, action) => {
      state.userPick = action.payload;
    },
    comChoice:(state,action) =>{
      state.comPick = action.payload;    
    },
    roundFinish:(state,action) =>{
      state.roundFinish = action.payload;
    },
    roundPlay:(state,action) =>{
      state.roundPlay = state.roundPlay + 1;
    },
    nextRound:(state,action)=>{
      state.winner = "VS";
      state.userPick = "";
      state.comPick = "";
      state.roundFinish = false;
    },
    gameFinish:(state,action)=>{
      state.gameFinish = true;
      state.roundFinish = true;
    },
    hasil:(state,action)=>{
      state.winner = action.payload;
      }, 
    win:(state,action)=>{
      state.winner = "You Win";
      state.score = state.score + 1;
    }, 
    lose:(state,action)=>{
      state.winner = "Com Win";
      state.comScore = state.comScore + 1;
    },
  },
});

export const { resetGame, userChoice , comChoice,roundFinish,roundPlay,nextRound,gameFinish,win,lose,hasil} = suitSlice.actions;
export default suitSlice.reducer;

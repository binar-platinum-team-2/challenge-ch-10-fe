import { createSlice } from "@reduxjs/toolkit";
const axios = require("axios");
const initialState = {
    userdata:[],
};

export const alldataSlice = createSlice({
  name: "alluser",
  initialState,
  reducers: {
    getUser: (state, action) => {
      state.userdata=(action.payload);
    }
  }
});

export const getUserAsync = () => async (dispatch) => {
  try {
    const response =   await axios.get("https://fsw-team2-backend.herokuapp.com/datauser");
    const hasil = response.data;
    const sortdata = hasil.sort((a, b) => (a.skor > b.skor ? -1 : 1));
    dispatch(getUser(sortdata));
  } catch (err) {
    alert("Axios Error , Can't Get Leaderboard Data");
  }
};

export const { getUser } = alldataSlice.actions;
export default alldataSlice.reducer;


// import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// import axios from "axios";

// export const getCompanies = createAsyncThunk(
//   "companyList/getCompanies", 
//   async () => {
//     try {
//       const response = await axios.get(
//         "https://mocki.io/v1/d4867d8b-b5d5-4a48-a4ab-79131b5809b8"
//       );
//       return response.data;
//     } catch (error) {
//       console.error(error);
//     }
// });

// const companySlice = createSlice({
//   name: "companyList",
//   initialState: {
//     company: {},
//     isLoading: false,
//     hasError: false
//   },
//   extraReducers: (builder) => {
//     builder
//       .addCase(getCompanies.pending, (state, action) => {
//       state.isLoading = true;
//       state.hasError = false;
//     })
//       .addCase(getCompanies.fulfilled, (state, action) => {
//         state.company = action.payload;
//         state.isLoading = false;
//         state.hasError = false
//       })
//       .addCase(getCompanies.rejected, (state, action) => {
//         state.hasError = true
//         state.isLoading = false;
//       })
//   }
// });

// // Selectors
// export const selectCompanies = state => state.companyList.company;
// export const selectLoadingState = state => state.companyList.isLoading;
// export const selectErrorState = state => state.companyList.hasError;

// export default companySlice.reducer;
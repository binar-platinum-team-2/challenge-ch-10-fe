import react from "react";
import Link from "next/link";

export default function Header() {
  const logoutHandler = async () => {
    localStorage.removeItem("accessToken");
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-dark rounded">
      <Link className="navbar-brand" href="/home">
        Gamepedia
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav end">
          <li className="nav-item active">
            <Link className="nav-link" href="/gamelist">
              Game List
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" href="/leaderboard">
              Leaderboard
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" href="/profile">
              Profile
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" onClick={logoutHandler} href="/">
              Logout
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}
